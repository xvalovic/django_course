from django.db import models

# Create your models here.
# Remember the three-step guide to making model changes:

 #    Change your models (in models.py).
 #    Run python manage.py makemigrations to create migrations for those changes
 #    Run python manage.py migrate to apply those changes to the database.



class data_vendor(models.Model):
	name = models.CharField(max_length=200)
	website_url = models.CharField(max_length=200, default=None, null=True, blank=True)
	created_date = models.DateTimeField('created_date')
	last_updated_date = models.DateTimeField('last_updated_date')



class symbol(models.Model):
	ticker = models.CharField(max_length=200)
	instrument = models.CharField(max_length=200)
	name = models.CharField(max_length=200)
	sector = models.CharField(max_length=200)
	currency = models.CharField(max_length=200, default=None, null=True, blank=True)
	created_date = models.DateTimeField('created_date')
	last_updated_date = models.DateTimeField('last_updated_date')


class daily_data(models.Model):
	data_vendor_id = models.ForeignKey(data_vendor, on_delete=models.PROTECT)
	symbol_id = models.ForeignKey(symbol, on_delete=models.PROTECT)
	created_date = models.DateTimeField('created_date')
	last_updated_date = models.DateTimeField('last_updated_date')
	date_price = models.DateTimeField('date_price')
	open_price = models.DecimalField(max_digits=20, decimal_places=10, default=0)
	high_price = models.DecimalField(max_digits=20, decimal_places=10, default=0)
	low_price = models.DecimalField(max_digits=20, decimal_places=10, default=0)
	close_price = models.DecimalField(max_digits=20, decimal_places=10, default=0)
	adj_close_price = models.DecimalField(max_digits=20, decimal_places=10, default=0)
	volume = models.IntegerField(default=0)


