from django.contrib import admin
from .models import data_vendor, symbol, daily_data

# Register your models here.
admin.site.register(data_vendor)
admin.site.register(symbol)
admin.site.register(daily_data)