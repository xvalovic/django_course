from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from .models import symbol, daily_data
import statistics

import datetime

# Create your views here.


def index(request):
	# Design vystupu je hard-coded in python, je lepsie pouzit template system
	'''
	symbols_list = symbol.objects.order_by('-ticker')
	output = '<br>'.join([s.name for s in symbols_list])
	return HttpResponse(output)
	'''
	# Vyuzitie template
	symbols_list = symbol.objects.order_by('ticker')
	context = {
		'symbols_list': symbols_list
	}
	return render(request, 'stocks/index.html', context)


def detail(request, symbol_id):
	# Ak nie je zvoleny param, zobraz posledne dva roky dodnes
	date_from = request.POST['date_from'] if 'date_from' in request.POST else str((datetime.datetime.now() - datetime.timedelta(days=365*3)).date())
	date_to = request.POST['date_to'] if 'date_to' in request.POST else str(datetime.datetime.now().date())

	symbol_info = get_object_or_404(symbol, id=symbol_id)
	data = daily_data.objects.filter(symbol_id=symbol_id, date_price__range=[date_from, date_to]).order_by('date_price')
	if data:
		aggr = {
			'open_price_mean': statistics.mean([d.open_price for d in data]),
			'close_price_mean': statistics.mean([d.close_price for d in data]),
			'high_price_mean': statistics.mean([d.high_price for d in data]),
			'low_price_mean': statistics.mean([d.low_price for d in data]),
			'adj_close_price_mean': statistics.mean([d.close_price for d in data]),
			'volume_mean': statistics.mean([d.volume for d in data])
		}
	else:
		aggr = {}

	return render(request, 'stocks/details_bt.html', {'symbol_info': symbol_info, 'data': data, 'aggr': aggr})
